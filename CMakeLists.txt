idf_component_register(SRCS "src/connection.c" "src/async.c" "src/ierror.c"
	"src/password.c" "src/response.c" "src/recv.c" "src/parser.c" "src/sync.c"
	"src/send.c" "src/run.c" "src/quote.c" "src/settings.c" "src/socket.c"
	"src/resolver.c" "src/fd_util.c" "src/error.c" "src/status.c" "src/list.c"
	"src/cstatus.c" "src/player.c" "src/audio_format.c" "src/mixer.c"
	INCLUDE_DIRS "include" "output"
)
